import { pizzaUser, pizzaBD } from "./data-pizza.js";

const tableSauce = document.querySelector(".table__sauce");
const tableTopping = document.querySelector(".table__topping");
const [...draggable] = document.getElementsByClassName("draggable");
const target = document.getElementById("target");

const pizzaSelectSize = (e) => {
    if(e.target.tagName === "INPUT" && e.target.checked){
        pizzaUser.size = pizzaBD.size.find(el=>el.name === e.target.id);
    };
    show(pizzaUser);
};

draggable.forEach((el) => {
    el.addEventListener('dragstart', (evt) => {
        el.parentElement.style.outline = "3px solid var(--main)";
        el.parentElement.style.borderRadius = "25px";
        evt.dataTransfer.effectAllowed = "move";
        evt.dataTransfer.setData("Text", el.id);
    }, false);

    el.addEventListener('dragend', () => {
        el.parentElement.style.outline = "";
        el.parentElement.style.borderRadius = "";
    }, false);
});

target.addEventListener("dragenter",  () => {
    target.style.transform = "scale(1.1)";
}, false);

target.addEventListener("dragleave",  () => {
    target.style.transform = "";
}, false);

target.addEventListener("dragover",  (evt) => {
    if (evt.preventDefault) evt.preventDefault();
    return false;
}, false);

target.addEventListener("drop",  (evt) => {

    // прекращаем дальнейшее распространение события по дереву DOM и отменяем возможный стандартный обработчик установленный браузером.
    if (evt.preventDefault) evt.preventDefault();
    if (evt.stopPropagation) evt.stopPropagation();

    target.style.transform = "";

    const e = evt.dataTransfer.getData("Text"); // получаем информации которая передавалась через операцию drag & drop 

    const elem = document.getElementById(e);
    // добавляем элемент в целевой элемент. Так как в DOM не может быть два идентичных элемента - элемент удаляется со своей старой позиции.

    let tempElName = pizzaBD.sauce.find(el => el.name === elem.id);

    if (tempElName != undefined) {
        pizzaUser.sauce = tempElName;

        if (elem.tagName === "IMG") {
            tableSauce.innerHTML = `<img src="${elem.src}">`;
        };
    } else {
        
        tempElName = pizzaBD.topping.find(el => el.name === elem.id);
        if (tempElName != undefined) {
            //перевіряємо, чи замовляли вже такий топінг
            if (pizzaUser.topping.find(el => el.name === tempElName.name)) {
                pizzaUser.topping.forEach((el) => {
                    if (el.name === tempElName.name) {el.count++}
                });
            } else {
                tempElName.count = 1;
                if (elem.tagName === "IMG") {
                    tempElName.source = elem.src
                }
                pizzaUser.topping.push(tempElName);
            };

         };
    };
        
    show(pizzaUser);


    return false;
}, false);

function show (pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping");

    let totalPrice = 0;
    if(pizza.size !== ""){
        totalPrice += parseFloat(pizza.size.price);
    }
    if(pizza.sauce !== ""){
        totalPrice += parseFloat(pizza.sauce.price);
    }
    if(pizza.topping.length){
        totalPrice += pizza.topping.reduce((a, b) => a + b.price * b.count, 0);
    }
    price.innerText = totalPrice;
   
    if(pizza.sauce !== ""){
        sauce.innerHTML = `<span class="topping">${pizza.sauce.productName}</span>`;
    }

    if (Array.isArray(pizza.topping)) {
        topping.innerHTML = pizza.topping.map((el) => (
            `<div>
                <span class="count-topping">${el.count}</span>
                <span class="minus-topping">-</span>
                <span class="topping">${el.productName}</span>
                <span class="del-all-topping">X</span>
            </div>`
        )).join("");

        tableTopping.innerHTML = pizza.topping.map((el) => (
            `<img src="${el.source}">`
        )).join("");
    };

    activateButtons()

    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date();
}

function activateButtons() {
    const [...minus] = document.getElementsByClassName("minus-topping");
    const [...del] = document.getElementsByClassName("del-all-topping");
   
    delButtons(minus);
    delButtons(del);

    function delButtons(arr) {
        arr.forEach((el) => {
            el.addEventListener('click', () => {
                let productName = el.parentElement.getElementsByClassName("topping")[0].innerText;
                
                for (let i = 0; i < pizzaUser.topping.length; i++) {

                    if (pizzaUser.topping[i].productName === productName) {
                        document.getElementsByClassName(`${pizzaUser.topping[i].name}--${pizzaUser.topping[i].count}`)

                        if (el.className === "minus-topping") {
                            pizzaUser.topping[i].count--
                        } else if(el.className === "del-all-topping"){
                            pizzaUser.topping[i].count = 0;
                        };
                    };

                    if (pizzaUser.topping[i].count === 0) {
                        pizzaUser.topping.splice(i, 1);
                        i--;
                    };
                };
                
                show(pizzaUser);
            });
        });
    };

    
};

const validate = (pattern, value) => pattern.test(value);

const banner = document.getElementById("banner")

banner.addEventListener("mouseover", () => {
    banner.style.bottom = Math.random() * 80 + "%";
    banner.style.right = Math.random() * 80 + "%";
});





export { pizzaSelectSize, show, validate};